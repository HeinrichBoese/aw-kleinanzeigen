const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json())

const pool = require('./db').pool;

app.get("/api/entry", async function (req, res) { // Gibt die letzten 20 Einträge zurück bei dehnen die Beschreibung bereits auf 100 Zeichen reduziert wurde
    try {
        const result = await pool.query("SELECT id, titel, LEFT(beschr, 100) AS beschr FROM entry WHERE DATEDIFF(NOW(), created)<14 ORDER BY created DESC, id DESC LIMIT 20");
        res.json(result);
    }
    catch(err) {
        console.error(err);
        res.status(500).end();
    }
});
app.get("/api/entry/cities/", async function (req, res) { // gibt eine Liste der 20 am häufigsten vorkommenden Orten zurück {ort: string, anzahl: INT}
    try {
        const result = await pool.query("SELECT ort, COUNT(*) AS anzahl FROM entry WHERE DATEDIFF(NOW(), created)<14 GROUP BY ort ORDER BY anzahl DESC");
        res.json(result);
    }
    catch(err) {
        console.error(err);
        res.status(501).end();
    }
});
app.get("/api/entry/cities/:city", async function (req, res) { // gibt eine Liste der Anzeigen für die Stadt zurück 
    const city = '%'+req.params.city+'%';
    try {
        const result = await pool.query('SELECT id, titel, LEFT(beschr, 100) AS beschr FROM entry WHERE ort LIKE ? AND DATEDIFF(NOW(), created)<14 ORDER BY created DESC, id DESC LIMIT 20', [city]);
        res.json(result);
    }
    catch(err) {
        console.error(err);
        res.status(501).end();
    }
});
app.get("/api/entry/searchtitel/:titel", async function (req, res) { // Gibt die letzten 20 Einträge zurück bei dehnen die Suche in dem Titel übereinstimmt
    const titel = '%'+req.params.titel+'%';
    try {
        const result = await pool.query("SELECT id, titel, LEFT(beschr, 100) AS beschr FROM entry WHERE titel LIKE ? AND DATEDIFF(NOW(), created)<14 ORDER BY created DESC, id DESC LIMIT 20", [titel]);
        res.json(result);
    }
    catch(err) {
        console.error(err);
        res.status(500).end();
    }
}); 
// app.get("/api/entry/searchcity/:city", async function (req, res) { // Rausgeworfen da die AUtoverfollständigung benutzt wird // Gibt die letzten 20 Einträge zurück bei dehnen die Suche mit Teilen in dem Stadtnamen übereinstimmt
//     const city = '%'+req.params.city+'%';
//     try {
//         const result = await pool.query("SELECT * FROM (SELECT ort, COUNT(*) AS anzahl FROM entry GROUP BY ort) AS ort WHERE ort LIKE ? AND DATEDIFF(NOW(), created)<14 ORDER BY anzahl DESC LIMIT 20", [city]);
//         res.json(result);
//     }
//     catch(err) {
//         console.error(err);
//         res.status(500).end();
//     }
// });
app.get("/api/entry/:id", async function (req, res) { 
    const Id = req.params.id;
        try {
        const result = await pool.query(`SELECT * FROM entry WHERE id =?`,[Id]);
        res.json(result);
    }
    catch(err) {
        console.error(err);
        res.status(500).end();
    }
});


app.post("/api/entry", async function(req, res) {
    try{
        const entry = req.body;
        console.log([entry.titel, entry.beschr, entry.name, entry.nachname, entry.ort, entry.preis, entry.verhand, entry.email, entry.tel])
        // Eingabedaten:Datum(date); Titel(string); Beschreibung(string); Name(string); Ort(string); Preis(Nummer); Verhandlungsbasis(boolean); Kontakt Email; Kontakt Tel.;
        await pool.query(
            "INSERT INTO entry (titel, beschr, name, nachname, ort, preis, verhand, email, tel) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
            [entry.titel, entry.beschr, entry.name, entry.nachname, entry.ort, entry.preis, entry.verhand, entry.email, entry.tel]
        );
        res.status(201).end();
    }
    catch(err) {
        console.error(err);
        res.status(500).end();
    }
});

app.use(express.static('public'));
app.use('/jquery', express.static('node_modules/jquery/dist'));

app.listen(3000, function () {
    console.log("listening on http://127.0.0.1:3000");
});
