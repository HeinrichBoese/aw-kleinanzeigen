const mariadb = require("mariadb");
const pool = mariadb.createPool({
  host: "localhost",
  user: "root",
  password: "313373",
  connectionLimit: 5,
  database: "academy",
});

// setup DB
pool.query(`CREATE TABLE IF NOT EXISTS entry (
	id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	titel VARCHAR(80) NOT NULL,
  name VARCHAR(255) NOT NULL,
  nachname VARCHAR(255) NOT NULL,
	beschr TEXT NOT NULL,
	ort VARCHAR(255) NOT NULL DEFAULT '',
	preis DECIMAL(10,2) UNSIGNED NOT NULL DEFAULT 0,
	verhand TINYINT UNSIGNED NOT NULL DEFAULT 0,
	email VARCHAR(255) NOT NULL DEFAULT 'keine Email hinterlegt',
	tel BIGINT UNSIGNED NULL DEFAULT NULL,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp(),
	PRIMARY KEY (id))`
	)
.then(() => console.info('Database initizalied'))
.catch(err => {
  console.error('Could not initialize database', err);
  process.exit();
});

module.exports = { pool };