let dataRes;
let splitted
let dataProp;
let stadtliste = [];
$( document ).ready(() => {
    $('.trigger').click (() => {
        dataProp = this.event.target.dataset.prop
    })
    
    $('.Ort').click(() => {
        $('.Ort').addClass("is-active")
        $('.Titel').removeClass("is-active")
    })
    $('.Titel').click(() => {
        $('.Titel').addClass("is-active")
        $('.Ort').removeClass("is-active")
    })

    $(".column").click(() => {
    dataProp = this.event.target.dataset.prop;
    if (dataProp == '') {
        return;
    }
    getDetail(dataProp)
    })
    $('#hauptuebersicht').click(() => {
        $('#mainSite').removeClass('is-hidden');
        $('#detailAnzeige').addClass('is-hidden');
    })
    $('#neueAnzeige').click(() => {
        window.location.assign("./inputForm.html") 
    })
    $('.Ort').click(() => {
        $("#input").autocomplete({
            disabled: false
        });
        $('.Ort').addClass("is-active")
        $('.Titel').removeClass("is-active")
    })
    $('.Titel').click(() => {
        $("#input").autocomplete({
            disabled: true
        });
        $('.Titel').addClass("is-active")
        $('.Ort').removeClass("is-active")
    })
    $('#checkFilter').click(() => {
        let activeSelector =  $('.is-active').html()
        if (activeSelector === '') {
            return
        }
        let inputData = String($('#input').val()).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
        switch (activeSelector) {
            case 'Titel':
                getTitel(inputData);
                return;
            case 'Ort':
                getOrt(inputData);
                return;
        }
    })

    $('ol').click(() => {
        let itemClicked = this.event.target.id
        getOrt(itemClicked)
    })

    $('#resetFilter').click(() => {
        $('#input').val('')
        $('.Ort').addClass("is-active")
        $('.Titel').removeClass("is-active")
        loadEntries()
    })

    $('#input').autocomplete({
        maxShowItems: 10, 
        source: stadtliste
    })
 
    function getDetail(dataProp) {
        if (dataProp == undefined) {
            return
        }
        
    $('#mainSite').addClass('is-hidden');
    $('#detailAnzeige').removeClass('is-hidden');
    $.get(`/api/entry/${dataProp}`, (response) => {
        responseData = response
    let Titel;
    let Name;
    let Vorname;
    let Beschreibung;
    let Ort;
    let Preis;
    let Verhand;
    let Email;
    let Tel;
    let timeStamp;
        responseData.forEach((obj) => {
            for (key in obj) {
                if (key == 'id') {
                    continue;
                }
                else if (key == 'titel') {
                    Titel = obj[key]
                }
                else if (key == 'nachname'){
                    Name = obj[key]
                }

                else if (key == 'name'){
                    Vorname = obj[key]
                }

                else if (key == 'beschr'){
                    Beschreibung = obj[key]
                }

                else if (key == 'ort'){
                    Ort = obj[key]
                }

                else if (key == 'preis'){
                    Preis = obj[key]
                }
                else if (key == 'verhand') {
                    if (obj[key] == 0) {
                        Verhand = 'Festpreis'
                    }
                    else if (obj[key] == 1){
                        Verhand = 'VHB'
                    }
                    else { Verhand = 'Zu verschenken'}
                }

                else if (key == 'email'){
                    Email = `<a href = "mailto:${obj[key]}" target = '_blank' style = "color:#3273dc"> ${obj[key]}</a>`
                }

                else if (key == 'tel' && obj[key] != ''){
                    Tel = '+49' + obj[key]
                }

                else if (key == 'created') {
                   timeStamp = obj[key] 
                   splitted = timeStamp.split('T') 
                }
                
            }
        })

        $('#title').html(Titel);
        $('#nachname').html(Name);
        $('#vorname').html(Vorname);
        $('#beschreibung').html(Beschreibung);
        $('#ort').html(Ort);
        if (Verhand == 'Zu verschenken' ) {
            $('#preis').html(Verhand);
        }
        else if (Number.isInteger(Preis)) {
            Preis =  Verhand +' ' +Preis + '.00' + '€' 
            $('#preis').html(Preis);
        }
        else {
            Preis =  Verhand +' ' +Preis + '€' 
            $('#preis').html(Preis);
        }
        $('#email').html(Email);
        $('#telefonnummer').html(Tel);
        $('#date').html(splitted[0]);

        })
    }


    function loadEntries() {
        $.get(`/api/entry`, (data) => {
            fillEntries(data)

        })
    }

    function fillEntries(data) {
        $('.adContent').html('')
        let id;
        let Titel;
        let Vorname;
        let Nachname;
        let Beschreibung;
        data.map(obj => {
            for (key in obj) {
                switch(key) {
                    case 'id' :
                      id = obj[key];
                      break;
                    case 'titel':
                      Titel = obj[key];
                      break;
                    case 'beschr':
                      Beschreibung = obj[key];
                      break;
                    case 'name':
                      Vorname = obj[key];
                      break;
                    case 'nachname':
                      Nachname = obj[key];
                      break;
                }
            }
        
        
        $('.adContent').append(`<div class = "card trigger" data-prop = "${id}"><div class="card-content"  data-prop = "${id}">
        <div class="media"  data-prop = "${id}">
          <div class="media-content"  data-prop = "${id}">
            <p class="label is-medium is-4 " data-prop = "${id}">${Titel}</p></div></div>
            <div class="content"  data-prop = "${id}">${Beschreibung}
            </div>
            </div>
            </div>`)
        }) 
    }
    

    function getOrt(city) {
        $.get(`/api/entry/cities/${city}`, (data) => {
            fillEntries(data)
        })
    }

    function getTitel(titel) {
        $.get(`/api/entry/searchtitel/${titel}`, (data) => {
            fillEntries(data)
        })
    }


    function fillTopFive() {
        let count = 0;
        $('#topFive').html('')
        $.get(`/api/entry/cities/`, (data) => {
            data.map(obj => {
                count ++;
            for (key in obj) {
                if (key === 'ort') {
                    stadtliste.push(obj[key])
                    if (count <= 5) {
                    $('#topFive').append(`<li id = "${obj[key]}"><a class = "topFiveCity" id = "${obj[key]}">${obj[key]}</a></li>`)   
                    }  
                }
            }
            })
        })
    }
    
    fillTopFive()
    loadEntries()

    function submitEntry(entry) {
        const headers = { 'Content-Type': 'application/json' };
        return fetch('/api/entry', { method: 'POST', headers, body: JSON.stringify(entry) });
    }


    $('#guestbook_form').on('submit', async (event) => {
        event.preventDefault();
        const formData = new FormData(event.target);
        const entry = Object.fromEntries(formData.entries());
        const response = await submitEntry(entry);
        if(response.ok) {
            event.target.reset();
            const newEntries = await loadEntries();
            showEntries(newEntries);
        }
    });

});