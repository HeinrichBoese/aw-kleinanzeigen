$(document).ready(function(){
    $('#hauptuebersichtInput').click(() => {
        window.location.assign("./index.html")
    })

    $.ajaxSetup({
        contentType: "application/json; charset=utf-8"
    });
    var nameReg = /^[A-Za-zäöü]+$/;
    var numberReg =  /^[0-9,.]+$/;
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    var preisVal;
    var vorName = $('#inputVorname');
    var name = $('#inputNachname');
    var titel = $('#inputTitel');
    var ort = $('#inputOrt');
    var beschreibung = $('#inputBeschreibung');
    var tel = $('#inputTelefon');
    var email = $('#inputemail');
    var preis = $('#preisInput');
    var checkedPreisAuswahl;
    
    var messageName = $('#vornameCheckExists');
    var messageNachname = $('#nameCheckExists');
    var messageTitel = $('#titleCheckExists');
    var messageOrt = $('#ortCheckExists');
    var messageKontakt = $('#kontaktCheckExists');
    var messageMail = $('#kontaktCheckExistsMail');
    var message = $('#beschreibungCheckExists');
    var messagePreis = $('#preisCheckExists');

    // Validation Vorname
    vorName.focusout(function(){
            if(vorName.val().length == 0){
                messageName.html("Bitte geben Sie einen Namen ein");
                messageName.addClass("has-background-danger");
            };
    });
    vorName.keyup(function(){
        if(!nameReg.test(vorName.val())){
            messageName.html("Trage Buchstaben ein.");
            messageName.addClass("has-background-danger");
        }else {
            messageName.html("");
        }
        if(vorName.val().length == 0){
            messageName.html("Bitte geben Sie einen Namen ein");
            messageName.addClass("has-background-danger");
        } 
    });

    // Validation Nachname
    name.focusout(function(){
        if(name.val().length == 0){
            messageNachname.html("Bitte geben Sie einen Namen ein");
            messageNachname.addClass("has-background-danger");
        };
    });
    name.keyup(function(){
        if(!nameReg.test(name.val())){
            messageNachname.html("Trage Buchstaben ein.");
            messageNachname.addClass("has-background-danger");
        } else {
            messageNachname.html("");
        }
        if(name.val().length == 0){
            messageNachname.html("Bitte geben Sie einen Namen ein");
            messageNachname.addClass("has-background-danger");
        };
    });

    // Validation Titel
    titel.focusout(function(){
        if(titel.val().length == 0){
            messageTitel.html("Bitte geben Sie einen Titel ein");
            messageTitel.addClass("has-background-danger");
        };
    });
    titel.keyup(function(){
        if(titel.val().length == 80 ){
            messageTitel.html("Der Titel darf maximal 80 Zeichen enthalten.");
            messageTitel.addClass("has-background-danger");
        } else {
            messageTitel.html("");
            messageTitel.removeClass("has-background-danger");
        };
    });

    // Validierung Ort
    ort.focusout(function(){
        if(ort.val().length == 0){
            messageOrt.html("Bitte geben Sie einen Ort ein");
            messageOrt.addClass("has-background-danger");
        } else {
            messageOrt.html("");
            messageOrt.removeClass("has-background-danger");
        }
    });

    //Validierung der Telefonnummer
  /*  tel.focusout(function(){
        if(tel.val().length == 0){
            messageKontakt.html("Bitte geben Sie eine Telefonnummer ein");
            messageKontakt.addClass("has-background-danger");
        };
    }); */
    tel.keyup(function(){
        if(!numberReg.test(tel.val())){
            messageKontakt.html("Die Telefonnummer muss aus Zahlen bestehen");
            messageKontakt.addClass("has-background-danger");
        } else {
            messageKontakt.html("");
        }
        if(tel.val().length == 0){
            messageKontakt.html(""); 
        }
    });
    

    // Validierung der E-Mail
    email.keyup(function(){
        if(!emailReg.test(email.val())){
            messageMail.html("Die E-mail muss ein @ und eine Endung enthalten.");
            messageMail.addClass("has-background-danger");
        } else {
            messageMail.html("");
        }
        if(email.val().length == 0){
            messageMail.html(""); 
        }
    });
/*
    email.focusout(function(){
        if(email.val().length == 0){
            messageMail.html("Bitte geben Sie eine E-Mail-Adresse ein");
            messageMail.addClass("has-background-danger");
        }
    });
*/
    beschreibung.focusout(function(){
        if(beschreibung.val().length == 0){
            message.html("Bitte geben Sie eine Beschreibung ein")
            message.addClass("has-background-danger");
        } else {
            message.html("");
        };
    });
    beschreibung.keyup(function(){
        if(beschreibung.val().length == 0){
            message.html("Bitte geben Sie eine Beschreibung ein")
            message.addClass("has-background-danger");
        } else {
            message.html("");
        }
    });

    //validierung des Preises
    $('#verschenken').click(function(){
            $('#festpreis').removeAttr("checked");
            $('#verhandlungsbasis').removeAttr("checked");
            messagePreis.hide();
            // $('#inputPreis').hide();
            // $('#preis-icon').hide();
            $('#inputPreis').addClass('is-hidden');
            // $('#preis-icon').addClass('is-hidden');
    });
    $('#festpreis').click(function(){
        $('#verschenken').removeAttr("checked");
        $('#verhandlungsbasis').removeAttr("checked");
        $('#inputPreis').removeClass('is-hidden');
        // $('#preis-icon').show();
    });
    $('#verhandlungsbasis').click(function(){
        $('#verschenken').removeAttr("checked");
        $('#festpreis').removeAttr("checked");
        $('#inputPreis').removeClass('is-hidden')
        // $('#preis-icon').show();
    });
    
    $('#agbcheck').click(() => {
        if ( $('#agbcheck').is(':checked')) {
        $('#agbCheckExists').html("")
        $('#agbCheckExists').removeClass("has-background-danger");
        }
    })

    $('#preisCheck').click(() => {
        if ( $('#festpreis').is(':checked') || $('#verhandlungsbasis').is(':checked') || $('#verschenken').is(':checked')) {
            $('#preisCheckExists').html("")
            $('#preisCheckExists').removeClass("has-background-danger");
            }
    })

    preis.keyup(function(){
        if(!numberReg.test(preis.val())){
            messagePreis.show();
            messagePreis.html("Bitte tragen Sie Zahlen ein.");
            messagePreis.addClass("has-background-danger");
        } else {
            messagePreis.html("");
        }
        if(preis.val().length == 0){
            console.log(preis.val().length)
            messagePreis.show();
            messagePreis.html("Bitte geben Sie einen Preis ein, oder verschenken Sie Ihr Produkt.");
            messagePreis.addClass("has-background-danger");
        }
    });

    // preis.focusout(function(){
    //     if(preis.val().length == 0){
    //         messagePreis.show();
    //         messagePreis.html("Bitte geben Sie einen Preis ein, oder verschenken Sie Ihr Produkt.");
    //         messagePreis.addClass("has-background-danger");
    //     }
    // });

    function createAd(e){
        if(vorName.val().length == 0){
            messageName.html("Bitte geben Sie einen Namen ein");
            messageName.addClass("has-background-danger");
            return;
        } else if(!nameReg.test(vorName.val())){
            messageName.html("Trage Buchstaben ein.");
            messageName.addClass("has-background-danger");
            return;
        } else if(name.val().length == 0){
            messageNachname.html("Bitte geben Sie einen Namen ein");
            messageNachname.addClass("has-background-danger");
            return;
        } else if(!nameReg.test(name.val())){
            messageNachname.html("Trage Buch)staben ein.");
            messageNachname.addClass("has-background-danger");
            return;
        } else if(titel.val().length == 0){
            messageTitel.html("Bitte geben Sie einen Titel ein");
            messageTitel.addClass("has-background-danger");
            return;
        } else if(ort.val().length == 0){
            messageOrt.html("Bitte geben Sie einen Ort ein");
            messageOrt.addClass("has-background-danger");
            return;
        } else if(!$('#festpreis').is(':checked') && !$('#verhandlungsbasis').is(':checked') && !$('#verschenken').is(':checked')) { 
            messagePreis.html("Wählen Sie aus, ob Ihr Produkt zu welchen Konditionen ihr Produkt verkauft werden soll.");
            messagePreis.addClass("has-background-danger");
            return;
        } else if(preis.val().length == 0 && !$('#verschenken').is(':checked')) {
            messagePreis.show();
            messagePreis.html("Bitte geben Sie einen Preis ein, oder verschenken Sie Ihr Produkt.");
            messagePreis.addClass("has-background-danger");
            return;
            
        } else if(tel.val().length == 0 && email.val().length == 0){
            messageKontakt.html("Bitte geben Sie eine Telefonnummer oder eine E-Mail-Adresse ein.");
            messageKontakt.addClass("has-background-danger");
            return;
        } else if(!numberReg.test(tel.val()) && tel.val() !== '') {
                messageKontakt.html("Die Telefonnummer muss aus Zahlen bestehen");
                messageKontakt.addClass("has-background-danger");
                return;
        } else if(!emailReg.test(email.val())){
            messageMail.html("Die E-mail muss ein @ und eine Endung enthalten.");
            messageMail.addClass("has-background-danger");
            return;
        } 
        else if(beschreibung.val().length == 0){
                message.html("Bitte geben Sie eine Beschreibung ein")
                message.addClass("has-background-danger");

        } else if(!$('#agbcheck').is(':checked')){
            $('#agbCheckExists').html("Bestätigen Sie die AGB")
            $('#agbCheckExists').addClass("has-background-danger");
            return;
        }
        else {
            return true;
        }
    };

    function checkChecked(){
    $("#preisCheck input:checked").each(function() {
        var some = this;
        if (some == festpreis){
            checkedPreisAuswahl = 0;
        } else if (some == verhandlungsbasis){
            checkedPreisAuswahl = 1;
        } else if (some == verschenken){
            checkedPreisAuswahl = 2;
            preisVal = 0;
        }
    });
    }

    function replaceKomma(){
        if (!$('#verschenken').is(':checked')) {
        preisVal = preis.val();
        if (preisVal === '') {
            return;
        }
        if (preisVal.indexOf(",") >= 0){
            preisVal = preisVal.replace(',', '.');   
        }
        if (!preisVal.includes('.')) {
            preisVal = preisVal + '.00';
        }   
        }
        preisVal = parseFloat(preisVal);
        preisVal = preisVal.toFixed(2);
        return true;
    }

    $('#signup').click(function(e){
        let created = createAd();
        let replaced = replaceKomma();
        let checked = checkChecked();
        if (replaced === true &&  created === true) {
        let data = new Object();
        data.titel = String(titel.val()).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
        data.beschr = String(beschreibung.val()).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
        data.name = vorName.val();
        data.nachname = name.val();
        data.ort = ort.val();
        data.preis = preisVal;
        data.verhand = checkedPreisAuswahl;
        data.email = email.val();
        data.tel = tel.val();
        data = JSON.stringify(data)
        console.log(data)
        
        $.post('/api/entry', data, (response) => {
            console.log(response)
            window.location.assign('./index.html')
            
        });
        }
    }); 
});